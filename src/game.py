import random
from typing import Sequence

import pygame


class Game:
    def __init__(self, vsync: bool = False) -> None:
        pygame.init()
        self._running = True
        self._display = None
        self._scenes = dict()
        self._current_scene = None
        self._clock = pygame.time.Clock()
        # display parameters
        self._resolution = (640, 480)
        self._vsync = 0 if vsync else 1
        self._display = pygame.display.set_mode(
            size=self._resolution,
            vsync=self._vsync
        )
        pygame.display.set_caption('Game')

    def add_scene(self, scene: pygame.sprite.RenderPlain, scene_name: str):
        self._scenes.update({scene_name: scene})
        if self._current_scene is None:
            self._current_scene = scene

    def _on_event(self, event: pygame.event.Event) -> None:
        if event.type == pygame.QUIT:
            self._running = False

    def _on_keypress(self, keys: Sequence[bool]):
        player = self._current_scene.get_player()
        if player is not None:
            player.process_input(keys)

    def _on_loop(self):
        pass

    def _on_render(self) -> None:
        self._display.fill((0, 0, 0))
        self._current_scene.draw(self._display)
        pygame.display.update()

    def _on_cleanup(self):
        pass

    def _on_execute(self):
        while self._running:
            for event in pygame.event.get():
                self._on_event(event)
            keys = pygame.key.get_pressed()
            self._on_keypress(keys)

            self._on_loop()
            self._on_render()
            self._clock.tick(3)

        self._on_cleanup()

    def run(self):
        self._on_execute()
