from typing import Sequence

import pygame


class PlayerControlSettings:
    GO_UP = pygame.K_w
    GO_DOWN = pygame.K_s
    GO_LEFT = pygame.K_a
    GO_RIGHT = pygame.K_d


class Player(pygame.sprite.Sprite):
    def __init__(self,
                 size: int = 32,
                 position: tuple = (0, 0)):
        super().__init__()
        self.controls = PlayerControlSettings()
        self._size = size
        self.velocity = self._size
        self.image = pygame.image.load('res/player1.png')
        self.rect = self.image.get_rect()
        self.rect.x = position[0]
        self.rect.y = position[1]

    def process_input(self, keys: Sequence[bool]):
        if keys[self.controls.GO_UP]:
            self.rect.y -= self.velocity

        elif keys[self.controls.GO_DOWN]:
            self.rect.y += self.velocity

        elif keys[self.controls.GO_LEFT]:
            self.rect.x -= self.velocity

        elif keys[self.controls.GO_RIGHT]:
            self.rect.x += self.velocity
