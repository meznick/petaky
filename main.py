import pygame

from src.game import Game
from src.player import Player


class PlayableScene(pygame.sprite.RenderPlain):
    def get_player(self):
        for sprite in self.sprites():
            if isinstance(sprite, Player):
                return sprite


if __name__ == '__main__':
    g = Game()
    scene = PlayableScene()
    scene.add(Player(32, (320, 240)))
    g.add_scene(scene, 'test')
    g.run()
